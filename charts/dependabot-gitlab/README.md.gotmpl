{{ template "chart.header" . }}
{{ template "chart.deprecationWarning" . }}

{{ template "chart.versionBadge" . }}{{ template "chart.typeBadge" . }}{{ template "chart.appVersionBadge" . }}

{{ template "chart.homepageLine" . }}

*dependabot-gitlab* is application providing automated dependency updates for gitlab projects.

## Introduction

This chart bootstraps dependabot-gitlab deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

This documentation is rendered from the `main` branch. To see docs for the latest release version, follow this link:
[{{ template "chart.version" . }}](https://gitlab.com/dependabot-gitlab/chart/-/tree/v{{ template "chart.version" . }}/README.md)

## Add Helm repo

```bash
helm repo add dependabot https://dependabot-gitlab.gitlab.io/chart
```

## Installing the Chart

Install this chart using:

```bash
helm install dependabot dependabot/dependabot-gitlab --values values.yaml
```

The command deploys dependabot-gitlab on the Kubernetes cluster in the default configuration. The [values](#values) section lists the parameters that can be configured during installation.

### MongoDb and Redis

By default, both `redis` and `mongodb` charts are deployed.
Installation of databases can be disabled by setting `redis.enabled: false` and `mongodb.enabled: false` in `values.yaml`.

**Both redis and mongodb charts require for password to be set, otherwise subsequent upgrades will fail!**

For more information on `mongodb` and `redis` chart configuration options, consult chart documentation:

- https://github.com/bitnami/charts/tree/master/bitnami/mongodb
- https://github.com/bitnami/charts/tree/master/bitnami/redis

{{ template "chart.sourcesSection" . }}

{{ template "chart.requirementsSection" . }}

{{ template "chart.valuesSection" . }}

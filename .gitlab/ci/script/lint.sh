#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

update_dependencies

log_with_header "Lint chart template files"
for yaml in .gitlab/ci/kube/values/*.yaml; do
  log_with_header "Validating $CHART_DIR with $yaml" "-"

  helm lint $CHART_DIR --strict --values $yaml
done
